'''
creating a csv file contains all the tables from 2009 to 2018
'''

import csv


data_path = '../data_arch/allTables.csv'
data_out = '../data/tables/'
start_year = 2009
end_year = 2018
def read_file(path):
    with open(path, 'r') as f:
        data = [row for row in csv.reader(f.read().splitlines())]
    return data

all_tables = read_file(data_path)
title = all_tables[0]
title = title[0].split('\t')
all_tables = all_tables[1:]
year = start_year
while year <= end_year:
    curr_table = all_tables[:20]
    with open(data_out +'table' + str(year) + '.csv', 'wb') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',')
        spamwriter.writerow(title)
        for entry in curr_table:
            entry = entry[0].split('\t')
            spamwriter.writerow(entry)
        
    year += 1
    all_tables = all_tables[20:]