'''
Adding team id to player stats 
'''

from sets import Set
import csv
from src.common.constants import years, pos_list

teams_names_maps = {}
teams = Set()
data_path = '../data_arch/players_stat/'
teams_path = './teams.csv'
teams = {}
alaa = Set()


def is_sub_str(str, sub_str_list):
    '''
    :param str: string
    :param sub_str_list: sub-strings list
    :return: true if there is a sub-string of str in sub_str_list
    '''

    for sb in sub_str_list:
        a = str.split(sb)
        if(len(a) > 1 and len(a[0]) > 0):
            return True
    return False


def fix_name_pos(row):
    '''

    fixing the position col.
    '''
    name = row[2]
    player_pos = []
    
    for p in pos_list:
        name_split = name.split(p)
        if len(name_split) > 1 and not is_sub_str(name_split[0],pos_list):
            player_pos += [p]
            for i in range(1,len(name_split)):
                curr_pos = name_split[i].split(' ')
                player_pos += curr_pos 
            if(name_split[0][-1] == 'L'):
                player_pos += ['L']
                name_split[0] = name_split[0][:-1]
            if(len(player_pos) > 1):
                row.append(' '.join(player_pos))
            else:
                row.append(player_pos[0])
            row[2] = name_split[0]
            return row
    return row


def read_file(path):
    '''
    reading csv file.
    '''
    with open(path, 'r') as f:
        data = [row for row in csv.reader(f.read().splitlines())]
    return data


def load_teams():
    '''
    loading the team dict
    '''

    if(len(teams) != 0):
        return

    teams_csv = read_file(teams_path)
    for row in teams_csv:
        teams[row[0]] = row[1]
        

def write_teams_names():
    '''
    writing team_id, team_name map to csv file
    '''
    load_teams()
    curr_teams = Set()
    for i in ['1','2']:
        for year in years: 
            rows = read_file(data_path + 'fifa{Y}_{I}.csv'.format(Y=year, I=i))
            for row in rows:
                curr_teams.add(row[6].split('20')[0].split('Jun')[0].split('May')[0].split('19')[0].split(' 3')[0].split('Dec')[0])
                
    with open('../data/teams_maps.csv', 'wb') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',')
        for team in teams.keys():
            spamwriter.writerow([team,teams[team]])
            
        for team in list(curr_teams):
            spamwriter.writerow([team])
        
        
    
def check_name(name):
    '''
    checking some names contain latin letters
    '''
    if(len(name.split('ico Madrid')[0]) != 0):
        return (240,name)
    if(name.split('Deportivo') == 'RC '):
        return (242,name)
    if(len(name.split('Deportivo Ala')[1]) != 0):
        return (463,name)
    if(len(name.split('aga CF')[1]) != 0):
        return (573,name)
    if(len(name.split('CD Lega')[1]) != 0):
        return (100888,name)
    return 'ERROR'
    

def get_team_ID(team_name):
    '''
    returns the id of the team by its name
    '''
    rows = read_file('../data/teams_maps.csv')
    teams_1 = {}
    teams_2 = {}
    for row in rows:
        teams_1[row[0]] = row[1]
        teams_2[row[2]] = row[1]
    
    name = team_name.split('20')[0].split('Jun')[0].split('May')[0].split('19')[0].split(' 3')[0].split('Dec')[0]
    name = name
    if(name in teams_1.keys()):
        return (teams_1[name],name)
    
    if(name in teams_2.keys()):
        return (teams_2[name],name)
    
    return check_name(name)


def add_team_ids():
    '''
    adds team ids to the players stats
    '''

    for i in ['1','2']:
        for year in years: 
            rows = read_file(data_path + 'fifa{Y}_{I}.csv'.format(Y=year, I=i))
            first_flag = True
            with open('../data/players_stats/fifa{Y}_{I}.csv'.format(Y=year, I=i), 'wb') as csvfile:
                spamwriter = csv.writer(csvfile, delimiter=',')
                for row in rows:

                        if(first_flag):
                            first_flag = False
                            row[6] = "Team"
                            row.append("TeamID")
                            row.append("Pos")
                        else:
                            x = get_team_ID(row[6])
                            row[6] = x[1]
                            row.append(x[0])
                        
                        spamwriter.writerow(fix_name_pos(row))		

#exec
write_teams_names()
add_team_ids()