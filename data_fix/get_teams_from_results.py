'''
exports results_teams.csv file contains the teams names as they appear in results file.
'''

from sets import Set
import csv
from src.common.constants import years

teams = Set()
data_path = '../data_arch/results/'

def read_file(path):
    with open(path, 'r') as f:
        data = [row for row in csv.reader(f.read().splitlines())]
    return data



for year in years: 
    rows = read_file(data_path + 'results{Y}.csv'.format(Y=year))
    for row in rows:
        teams.add(row[2])
        teams.add(row[3])	

with open('results_teams.csv', 'wb') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=',')
    for team in sorted(teams):
        spamwriter.writerow([team,0])
