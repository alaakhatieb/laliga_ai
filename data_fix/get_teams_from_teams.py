'''
Saves all the teams as a file teams_teams.csv - pair name,id
'''
from sets import Set
import csv
from src.common.constants import years

teams = Set()
data_path = '../data_arch/teams/'

def read_file(path):
    with open(path, 'r') as f:
        data = [row for row in csv.reader(f.read().splitlines())]
    return data



for year in years: 
    rows = read_file(data_path + 'fifa{Y}_2.csv'.format(Y=year))
    for row in rows:
        teams.add((row[2].split("Spanish")[0], row[4]))

with open('teams_teams.csv', 'wb') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=',')
    for team in sorted(teams):
        spamwriter.writerow([team[0], team[1]])
