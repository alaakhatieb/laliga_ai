'''
1. Writing teams with fixed fields
2. Writing La Liga history with fixed fields
'''

from sets import Set
import csv
from src.common.constants import YEARS, TEAM_COLS
import re
data_path = '../data_arch/teams/'

cols_indx = []

def fix_team_name(team_name):
    return team_name.split('Spanish')[0]

def read_file(path):
    with open(path, 'r') as f:
        data = [row for row in csv.reader(f.read().splitlines())]
    return data

def get_market_value(value):
    pattern = re.compile('(0|[1-9][0-9]*)(M|K)$')
    values_array = None
    if pattern.search(value):
        values_array = re.split('(0|[1-9][0-9]*)(M|K)$',value)

    if not values_array:
        return None
    p = -1
    for i in range(0,len(values_array)):
        if values_array[i] == 'M' or values_array[i] == 'K':
            p = i
            break
    if p >0 :
        return str(values_array[p-1]) + values_array[p]
    return None



def load_cols(first_row):

    del cols_indx[:]

    for i in range(0,len(first_row)):
        if first_row[i] in TEAM_COLS:
            cols_indx.append(i)



def write_teams_csv():

    for i in ['1','2']:
        for year in YEARS:
            rows = read_file(data_path + 'fifa{Y}_{I}.csv'.format(Y=year, I=i))
            first_flag = True
            with open('../data/teams/fifa{Y}_{I}.csv'.format(Y=year, I=i), 'wb') as csvfile:
                spamwriter = csv.writer(csvfile, delimiter=',')
                for row in rows:

                        if(first_flag):
                            first_flag = False
                            load_cols(row)
                        else:
                            row[2] = fix_team_name(row[2])
                            row[-1] = get_market_value(row[-1])

                        updated_row = []
                        for indx in cols_indx:
                            updated_row.append(row[indx])

                        spamwriter.writerow(updated_row)
                        
def write_laliga_teams_history():
    rows = read_file(data_path + 'laliga_history.csv')
    first_row = True
    with open('../data/laliga_teams_history.csv', 'wb') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',')
        for row in rows:
            if first_row:
                first_row = False
                spamwriter.writerow(row)  
                continue

            if not row[2].isdigit() or int(row[2]) == -1:
                continue
            
            for i in range(10,len(row)):
                if not row[i].isdigit():
                    row[i] = '-'
                    
            spamwriter.writerow(row)  

write_teams_csv()
write_laliga_teams_history()