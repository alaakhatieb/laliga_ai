'''
Adding team ID to results data.
'''


import csv

firstline = True
ids = {}
years = ['07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18']
data_path = '../data_arch/results/'
def read_file(path,flag):
    with open(path, flag) as f:
        data = [row for row in csv.reader(f.read().splitlines())]
    return data

def getIndexes(row,header):
    hash = []
    for i in header:
        hash.append(row.index(i))
    return hash


table = read_file("../data_fix/teams.csv",'r')
for row in table:
    ids[row[2]] = row[1]

ids['HomeTeam'] ='HomeTeamId'
ids['AwayTeam'] ='AwayTeamId'
headers= ["Date" ,"HomeTeam" ,"AwayTeam" ,"FTHG" ,"FTAG" ,"HTHG" ,"HTAG" ,"BbAvH" ,"BbAvD" ,"BbAvA"]
for year in years:
    rows = read_file(data_path + 'results{Y}.csv'.format(Y=year),'r+')
    headerIndex = getIndexes(rows[0],headers)
    yearRow =[]
    yearRow.append(headers + ["HomeId","AwayId"])
    first = True
    for row in rows:
        if(first == True):
            first = False
            continue
        line = []
        for indx in headerIndex:
            line.append(row[indx])
        homeId = ids[row[headerIndex[1]]]
        awayId = ids[row[headerIndex[2]]]
        line.append(homeId) #= homeId
        line.append(awayId) #= awayId
        yearRow.append(line)

    with open('../data/results/results{Y}.csv'.format(Y=year), 'wb') as the_file:
        spamwriter = csv.writer(the_file, delimiter=',')
        for result in yearRow:
            spamwriter.writerow(result)
