'''
Generates dataset #2
'''
import csv
from src.common.constants import *
firstline = True
ids = {}
years = YEARS
data_path = '../data/results/'
teams_path = '../data/teams/'
players_stats = '../data/players_stats/'
OVA=4

def read_file(path,flag):
    with open(path, flag) as f:
        data = [row for row in csv.reader(f.read().splitlines())]
    return data

def check_contain(array1,array2):
    for item in array2:
        if item in array1:
            return True
    return False
def getBest(teamId,positions,year):
    stats_file = read_file(players_stats + 'fifa{Y}_2.csv'.format(Y=year),'r+')
    power_array=[]
    for row in stats_file[1:]:
        index = len(row)
        teamId_index = index - 2
        position_index = index - 1
        if int(row[teamId_index]) == int(teamId):
            # print position in row[position_index],row[position_index]
            if check_contain(positions,row[position_index].split()):
                power_array.append(row[OVA])
    # print power_array
    return  power_array

def get_powers(teamId,Y):
    attack_array = []
    defe_array = []
    mid_array = []
    gk_array = []
    defe_array =getBest(teamId, POST_DEF_LIST, Y)
    attack_array =getBest(teamId, POS_ATT_LIST, Y)
    mid_array = getBest(teamId, POS_MID_LIST, Y)
    gk_array = getBest(teamId, POS_GK_LIST, Y)
    defe_array = sorted(defe_array,reverse=True)
    attack_array = sorted(attack_array,reverse=True)
    mid_array = sorted(mid_array,reverse=True)
    gk_array = sorted(gk_array,reverse=True)
    #print defe_array,"----",attack_array,"-----",mid_array,"----",gk_array
    defAvg =0
    for i in range(0,min(len(defe_array),4)):
        defAvg += int(defe_array[i])
    attackAvg = 0
    for i in range(0,min(len(attack_array),3)):
        attackAvg += int(attack_array[i])
    midAvg = 0
    for i in range(0,min(len(mid_array),3)):
        midAvg += int(mid_array[i])
    gkAvg = 0
    for i in range(0,min(len(gk_array),1)):
        gkAvg += int(gk_array[i])
    best11 = gkAvg + midAvg + attackAvg + defAvg
    best4sub = 0
    if len(defe_array) > 0 and int(defe_array[:5][-1]):
        best4sub +=int(defe_array[:5][-1])
    if len(attack_array) > 0 and int(attack_array[:5][-2]):
        best4sub +=int(attack_array[:5][-2])
    if len(attack_array) > 0 and int(attack_array[:5][-1]):
        best4sub+= int(attack_array[:5][-1])
    if len(mid_array) > 0 and int(mid_array[:4][-1]):
        best4sub+= int(mid_array[:4][-1])

    data = {"defAvg": 0, "attackAvg":0, "midAvg": 0, "gkAvg": 0,
            "best4sub": 0, "best11": 0}
    if defAvg:
        data["defAvg"]=defAvg/4

    if attackAvg:
        data["attackAvg"]=attackAvg/3
    if midAvg:
        data["midAvg"]=midAvg/3
    if gkAvg:
        data["gkAvg"]=gkAvg
    if best4sub:
        data["best4sub"]=best4sub/4
    if best11:
        data["best11"]=best11/11
    # data={"defAvg":defAvg/4,"attackAvg":attackAvg/3,"midAvg":midAvg/3,"gkAvg":gkAvg,"best4sub":best4sub/4,"best11":best11/11}
    return data

for year in YEARS:
    results = read_file(data_path + 'results{Y}.csv'.format(Y=year),'r+')
    teams = read_file(teams_path + 'fifa{Y}_2.csv'.format(Y=year),'r+')
    yearRow= []
    headers = ["homeId", "homeOVA", "homeATT", "homeMID", "homeDEF", "awayId", "awayOVA", "awayATT", "awayMID",
               "awayDEF", "homeBest11", "homeBest4sub", "homeBest4def", "homeBest3mid"
        , "homeBest3att", "homeBestGK", "awayBest11", "awayBest4sub", "awayBest4def", "awayBest3med", "awayBest3att",
               "awayBestGK", "TAG"]
    yearRow.append(headers)
    first = True
    for row in results:
        if(first == True):
            first = False
            continue
        line = []
        # print(row)
        # break
        homeId = int(row[10])
        awayId = int(row[11])
        res = int(row[3]) - int(row[4])
        if(res == 0):
            res = 0
        elif res < 0:
            res = -1
        else:
            res = 1
        first = True
        for team in teams:
            line=[]
            if (first == True):
                first = False
                continue
            if(int(team[0]) == int(homeId)):
                homeOVA = team[1]
                homeATT = team[2]
                homeMID = team[3]
                homeDEF = team[4]
                line.append(homeId)
                line.append(homeOVA)
                line.append(homeATT)
                line.append(homeMID)
                line.append(homeDEF)
                break
        first = True
        for team1 in teams:
            if (first == True):
                first = False
                continue
            if (int(team1[0]) == int(awayId)):
                awayOVA = team1[1]
                awayATT = team1[2]
                awayMID = team1[3]
                awayDEF = team1[4]
                line.append(awayId)
                line.append(awayOVA)
                line.append(awayATT)
                line.append(awayMID)
                line.append(awayDEF)
                homeData =get_powers(homeId,year)
                awayData =get_powers(awayId,year)
                if homeData["best4sub"] == 0 or homeData['best11'] == 0:
                    homeData["best4sub"] = homeOVA
                    homeData['best11'] = homeOVA
                if homeData["defAvg"] == 0:
                    homeData["defAvg"] = homeDEF
                if homeData["midAvg"] == 0:
                    homeData["midAvg"] = homeMID
                if homeData["attackAvg"] == 0:
                    homeData["attackAvg"] = homeATT
                if homeData["gkAvg"] == 0:
                    homeData["gkAvg"] = homeOVA

                if awayData["best4sub"] == 0 or awayData["best11"] == 0:
                    awayData["best4sub"] = awayOVA
                    awayData["best11"] = awayOVA
                if awayData["defAvg"] == 0:
                    awayData["defAvg"] = awayDEF
                if awayData["midAvg"] == 0:
                    awayData["midAvg"] = awayMID
                if awayData["attackAvg"] == 0:
                    awayData["attackAvg"] = awayATT
                if awayData["gkAvg"] == 0:
                    awayData["gkAvg"] = awayOVA

                line.append(homeData['best11'])
                line.append(homeData["best4sub"])
                line.append(homeData["defAvg"])
                line.append(homeData["midAvg"])
                line.append(homeData["attackAvg"])
                line.append(homeData["gkAvg"])
                line.append(awayData["best11"])
                line.append(awayData["best4sub"])
                line.append(awayData["defAvg"])
                line.append(awayData["midAvg"])
                line.append(awayData["attackAvg"])
                line.append(awayData["gkAvg"])
                line.append(res)
                yearRow.append(line)
                break

    with open('../datasets/dataset2/data{Y}_2.csv'.format(Y=year), 'w') as the_file:
        spamwriter = csv.writer(the_file, delimiter=',')
        for result in yearRow:
            spamwriter.writerow(result)