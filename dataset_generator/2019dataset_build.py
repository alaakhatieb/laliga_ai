'''
Generates dataset for season 2018/19 - the current season
'''

import csv
from src.common.utils import get_weighted_data, skip_first_line, read_csv_file

data_path = '../data_season19/fifa19_'

headers = ["homeId", "homeOVA", "homeATT", "homeMID", "homeDEF", "awayId", "awayOVA", "awayATT", "awayMID", "awayDEF"]


data_1 = skip_first_line(read_csv_file(data_path + '1.csv'))
data_2 = skip_first_line(read_csv_file(data_path + '2.csv'))
data = get_weighted_data(data_1, data_2, 0.5, 0.5, [1,2,3,4])
games = []
count = 0
for row1 in data:
    for row2 in data:
        if row1[0] == row2[0]:
            continue
        row1 = [int(x) for x in row1]
        row2 = [int(x) for x in row2]
        games.append(row1 + row2)
        count += 1
with open('../datasets/2019dataset/2019dataset.csv', "w") as output:
    writer = csv.writer(output, lineterminator='\n')
    writer.writerow(headers)
    for row in games:
        writer.writerow(row)
