'''
Generates dataset #1
'''
import csv

firstline = True
ids = {}
years = ['07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18']
data_path = '../data/results/'
teams_path = '../data/teams/'
def read_file(path,flag):
    with open(path, flag) as f:
        data = [row for row in csv.reader(f.read().splitlines())]
    return data


headers = ["homeId", "homeOVA", "homeATT", "homeMID", "homeDEF", "awayId", "awayOVA", "awayATT", "awayMID", "awayDEF",
           "TAG"]


for year in years:
    results = read_file(data_path + 'results{Y}.csv'.format(Y=year),'r+')
    teams = read_file(teams_path + 'fifa{Y}_1.csv'.format(Y=year),'r+')
    yearRow= []
    headers = ["homeId", "homeOVA", "homeATT", "homeMID", "homeDEF","awayId", "awayOVA", "awayATT", "awayMID","awayDEF","TAG"]
    yearRow.append(headers)
    first = True
    for row in results:
        if(first == True):
            first = False
            continue
        line = []
        # print(row)
        # break
        homeId = int(row[10])
        awayId = int(row[11])
        res = int(row[3]) - int(row[4])
        if(res == 0):
            res = 0
        elif res < 0:
            res = -1
        else:
            res = 1
        first = True
        for team in teams:
            line=[]
            if (first == True):
                first = False
                continue
            if(int(team[0]) == int(homeId)):
                homeOVA = team[1]
                homeATT = team[2]
                homeMID = team[3]
                homeDEF = team[4]
                line.append(homeId)
                line.append(homeOVA)
                line.append(homeATT)
                line.append(homeMID)
                line.append(homeDEF)
                break
        first = True
        for team1 in teams:
            if (first == True):
                first = False
                continue
            if (int(team1[0]) == int(awayId)):
                awayOVA = team1[1]
                awayATT = team1[2]
                awayMID = team1[3]
                awayDEF = team1[4]
                line.append(awayId)
                line.append(awayOVA)
                line.append(awayATT)
                line.append(awayMID)
                line.append(awayDEF)
                line.append(res)
                yearRow.append(line)
                break


    with open('../datasets/dataset1/data{Y}_1.csv'.format(Y=year), 'wb') as the_file:
        spamwriter = csv.writer(the_file, delimiter=',')
        for result in yearRow:
            spamwriter.writerow(result)
