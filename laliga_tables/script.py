'''
exports results_teams.csv file contains the teams names as they appear in results file.
'''

from sets import Set
import csv

data_path = '../data_arch/results/'
import operator
def calcPoints(games):
    teams=dict()
    print("-------")
    for row in games:
        teams[str(row[0])]=0
    print(teams)
    for row in games:
        if(int(row[2]) < 0):
            teams[str(row[1])] += 3
        if(int(row[2]) > 0):
            teams[str(row[0])] += 3
        if(int(row[2]) == 0):
            teams[str(row[0])] += 1
            teams[str(row[1])] += 1
    return teams

# data07_2.csv  in datasets/dataset1
def getResults(path):
    data=[]
    print(path)
    with open(path, 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',')
        for rows in spamreader:
            home=rows[0]
            away=rows[5]
            res=rows[10]
            d= (home,away,res)
            data.append(d)
    return data

file_name='../datasets/dataset1/data{Y}_1.csv'
years=['09','10','11','12','13','14','15','16','17','18']

for y in years:
    name=file_name.format(Y=y)
    # name=name.format(V=1)
    resultsFormat = getResults(name);
# [(home,away,res)...]
    # resultsFormat = getResults('../datasets/dataset1/data07_2.csv')
    resultsFormat.pop(0)
    x=calcPoints(resultsFormat)
    sorted_x = sorted(x.items(), key=operator.itemgetter(1))
    # print("1", sorted_x)
    count=len(sorted_x)
    order=dict()
    for i in range(1,count+1):
        item = (sorted_x[i-1][0],sorted_x[i-1][1])
        order[i]=item #sorted_x[i-1][0]
        # order[str(i)] = (str(i),sorted_x[i-1][0])
    # print(order,">>>",)
    writeFiles='order{Y}.csv'
    fileWrite = writeFiles.format(Y=y)
    with open(fileWrite, 'wb') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',')
        spamwriter.writerow(["order", "teamId","points"])
        l=[]
        for team in order:
            print(team,order[team][0])
            l.append((len(order)-team+1, order[team][0], order[team][1]))
        for i in range(1,21):
            spamwriter.writerow([l[20-i][0], l[20-i][1],l[20-i][2]])

        print(l)
