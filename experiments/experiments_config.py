from src.common.constants import *
import copy

'''
SOME CONSTANTS FOR CONFIGURING EXPERIMENT
'''

DATA_PATH1 = '../datasets/dataset1/data'
DATA_PATH2 = '../datasets/dataset2/data'
DATA_PATH3 = '../datasets/dataset3/data'
DATA1_FIELDS = ['homeId', 'homeOVA', 'homeATT', 'homeMID', 'homeDEF','awayId', 'awayOVA', 'awayATT', 'awayMID', 'awayDEF','TAG']
WEIGHTED1_COLS = ['homeOVA', 'homeATT', 'homeMID', 'homeDEF', 'awayOVA', 'awayATT', 'awayMID', 'awayDEF']
DATA2_FIELDS = ["homeId", "homeOVA", "homeATT", "homeMID", "homeDEF", "awayId", "awayOVA", "awayATT", "awayMID",
               "awayDEF", "homeBest11", "homeBest4sub", "homeBest4def", "homeBest3mid"
        , "homeBest3att", "homeBestGK", "awayBest11", "awayBest4sub", "awayBest4def", "awayBest3med", "awayBest3att",
               "awayBestGK", "TAG"]
DATA3_FIELDS = ["homeId", "awayId", "homeBest4sub", "homeBest4def", "homeBest3mid", "homeBest3att",
                "homeBestGK", "awayBest4sub", "awayBest4def", "awayBest3med", "awayBest3att",
                "awayBestGK","TAG"]
WEIGHTED2_COLS = ["homeOVA", "homeATT", "homeMID", "homeDEF", "awayOVA", "awayATT", "awayMID",
               "awayDEF", "homeBest11", "homeBest4sub", "homeBest4def", "homeBest3mid",
        "homeBest3att", "homeBestGK", "awayBest11", "awayBest4sub", "awayBest4def", "awayBest3med", "awayBest3att",
               "awayBestGK"]
WEIGHTED3_COLS = ["homeBest4sub", "homeBest4def", "homeBest3mid", "homeBest3att",
                "homeBestGK", "awayBest4sub", "awayBest4def", "awayBest3med", "awayBest3att",
                "awayBestGK","TAG"]


CLASSIFIERS_PARAMS_EXP0 = {
                           DT: {'max_depth': [None], 'min_samples_leaf': [1],
                                'criterion': ['gini'], 'splitter': ['best']},
                           RANDOM_FOREST: {'n_estimators': [10],  'max_depth': [None],
                                           'min_samples_leaf': [1], 'criterion': ['gini'],
                                           'bootstrap': [True],  'max_features': ['auto']},
                        KNN: {'n_neighbors': [5], 'weights': ['uniform'], 'leaf_size': [30]}}

CLASSIFIERS_PARAMS_EXP1 = {
                           DT: {'max_depth': [None], 'min_samples_leaf': [1, 5, 10, 25, 50, 100],
                                'criterion': ['gini', 'entropy'], 'splitter': ['random', 'best']},
                           RANDOM_FOREST: {'n_estimators': [10],  'max_depth': [None],
                                           'min_samples_leaf': [1, 5, 10, 25, 50, 100], 'criterion': ['gini', 'entropy'],
                                           'bootstrap': [True, False],  'max_features': ['auto']},
     KNN: {'n_neighbors': [3, 6, 9, 12, 15, 18, 21, 24, 27, 30], 'weights': ['uniform', 'distance']}
}


CLASSIFIERS_PARAMS_EXP2 = {
                           DT: {'max_depth': [None, 5, 10, 50, 75, 100], 'min_samples_leaf':  [1],
                                'criterion': ['gini', 'entropy'], 'splitter': ['random', 'best']},
                            RANDOM_FOREST: {'n_estimators': [10], 'max_depth': [None, 5, 10, 20, 50, 75, 100],
                                'min_samples_leaf': [1], 'criterion': ['gini', 'entropy'], 'bootstrap': [True, False],
                                            'max_features': ['auto']}
}

CLASSIFIERS_PARAMS_EXP3 = {
    RANDOM_FOREST: {'n_estimators': [2, 5, 10, 15, 19, 25, 30], 'max_depth': [None],
                    'min_samples_leaf': [1], 'criterion': ['gini', 'entropy'],
                    'bootstrap': [True, False], 'max_features': ['auto']}
}

CLASSIFIERS_PARAMS_EXP4 = {
    RANDOM_FOREST: {'n_estimators': [10], 'max_depth': [None],
                    'min_samples_leaf': [1], 'criterion': ['gini', 'entropy'],
                    'bootstrap': [True, False], 'max_features': [0.1, 0.25, 0.5, 0.75, 0.9, 1.0]}
}

EXP0 = {'exp0_1': {DATA_PATH: DATA_PATH1, DATA_FIELDS: DATA1_FIELDS, DATA_PART_WEIGHT: 0.2, WEIGHTED_COLS: WEIGHTED1_COLS,
                          CLASSIFIERS_PARAMS: CLASSIFIERS_PARAMS_EXP0},
    'exp0_2': {DATA_PATH: DATA_PATH1, DATA_FIELDS: DATA1_FIELDS, DATA_PART_WEIGHT: 0.5, WEIGHTED_COLS: WEIGHTED1_COLS,
                      CLASSIFIERS_PARAMS: CLASSIFIERS_PARAMS_EXP0},
    'exp0_3': {DATA_PATH: DATA_PATH1, DATA_FIELDS: DATA1_FIELDS, DATA_PART_WEIGHT: 0.8, WEIGHTED_COLS: WEIGHTED1_COLS,
               CLASSIFIERS_PARAMS: CLASSIFIERS_PARAMS_EXP0}}

'''
=============================================================================================================================
'''

'''
EXPERIMENT:
    exp_name: 
            data_path, data_fields, data_part_weight*, weights_cols*, classifiers_params.
    
* - data_part_weight and weights_cols: the data we got from sofifa.com contains two parts for each year,
    part one stats from the begining of the season and part two stats from the middle of the season.
data_part_weight: the ration between part1 and part2
weights_cols: which columns have the ability to sum (for example: not id columns)

'''
#Data part weight: the first half of the season
EXPERIMENTS = {

    'exp1': {DATA_PATH: DATA_PATH1, DATA_FIELDS: DATA1_FIELDS, DATA_PART_WEIGHT: 0.5, WEIGHTED_COLS: WEIGHTED1_COLS,
                      CLASSIFIERS_PARAMS: CLASSIFIERS_PARAMS_EXP1},
    'exp2': {DATA_PATH: DATA_PATH1, DATA_FIELDS: DATA1_FIELDS, DATA_PART_WEIGHT: 0.5, WEIGHTED_COLS: WEIGHTED1_COLS,
                      CLASSIFIERS_PARAMS: CLASSIFIERS_PARAMS_EXP2},
    'exp3': {DATA_PATH: DATA_PATH1, DATA_FIELDS: DATA1_FIELDS, DATA_PART_WEIGHT: 0.5, WEIGHTED_COLS: WEIGHTED1_COLS,
               CLASSIFIERS_PARAMS: CLASSIFIERS_PARAMS_EXP3},
    'exp4': {DATA_PATH: DATA_PATH1, DATA_FIELDS: DATA1_FIELDS, DATA_PART_WEIGHT: 0.5, WEIGHTED_COLS: WEIGHTED1_COLS,
               CLASSIFIERS_PARAMS: CLASSIFIERS_PARAMS_EXP4},
               }


'''
exp_name_v2: the same of exp_name but with dataset2
exp_name_v3: the same of exp_name but with dataset3
'''

#second round: with data2:
expsv2 = copy.deepcopy(EXPERIMENTS)
#third round: with data3:
expsv3 = copy.deepcopy(EXPERIMENTS)

for key, val in expsv2.items():
    key += '_v2'
    val[DATA_PATH] = DATA_PATH2
    val[DATA_FIELDS] = DATA2_FIELDS
    val[WEIGHTED_COLS] = WEIGHTED2_COLS
    EXPERIMENTS[key] = val

for key, val in expsv3.items():
    key += '_v3'
    val[DATA_PATH] = DATA_PATH3
    val[DATA_FIELDS] = DATA3_FIELDS
    val[WEIGHTED_COLS] = WEIGHTED3_COLS
    EXPERIMENTS[key] = val
