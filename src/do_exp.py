from src.common import utils
from src.common.classifiers import get_classifier
from experiments.experiments_config import *
import itertools
from src.result_evaluation import tables_compare, special_tables_compare
import numpy
import os
import operator
import csv

'''
The engine of the experiments execution. running an experiment and writing its results to csv file
'''

def write_results(out_dir, results):

    out_file = out_dir + '/' + 'results.csv'
    with open(out_file, "w") as output:
        writer = csv.writer(output, lineterminator='\n')
        for val in results:
            writer.writerow(val)


def aggregate_results(results):
    special = 0
    groups = 0
    counter = 0

    for year, res in results.items():
        special += res['special_score']
        groups += res['groups_score']
        counter += 1

    return {'special_score': special/counter, 'groups_score': groups/counter}


def get_special_evaluate_results(predicted_table, real_table):
    weights = [3, 2, 1, 3] # x3 for first group, x2 for the second, x3 for the last and x1 for the middle-table teams
    return special_tables_compare(predicted_table, real_table, weights)


def get_groups_evaluate(predicted_table, real_table):
    resolution = 4
    uniform = (int(NUM_OF_TEAMS/resolution)) * [1]
    weights = [2] + uniform[1:-1] + [2] # split the table into 5 (20/4) groups, and x2 weight for the first and the last group
    return tables_compare(predicted_table, real_table, resolution, weights)


def run(exp):

    out_dir = '../' + OUT_DIR + '/' + exp
    if not os.path.exists(out_dir):
        os.mkdir(out_dir)
    weighted_indexes = utils.get_indexes(EXPERIMENTS[exp][DATA_FIELDS], EXPERIMENTS[exp][WEIGHTED_COLS])
    w = EXPERIMENTS[exp][DATA_PART_WEIGHT]
    homeId_index = EXPERIMENTS[exp][DATA_FIELDS].index("homeId")
    awayId_index = EXPERIMENTS[exp][DATA_FIELDS].index("awayId")
    for classifier in EXPERIMENTS[exp][CLASSIFIERS_PARAMS].keys():
        if not os.path.exists(out_dir + '/' + classifier):
            os.mkdir(out_dir + '/' + classifier)
        data = [x for x in EXPERIMENTS[exp][CLASSIFIERS_PARAMS][classifier].values()]
        params_values = list(itertools.product(*data))
        params_keys = EXPERIMENTS[exp][CLASSIFIERS_PARAMS][classifier].keys()
        result_to_write = [list(params_keys) + ['groups_score', 'special_score']]

        for params in params_values:
            p = dict(zip(params_keys, params))
            results = run_exp(EXPERIMENTS[exp][DATA_PATH], classifier, p, weighted_indexes,
                               homeId_index, awayId_index, w)
            agg_res = aggregate_results(results)
            curr_params_values = ['default' if v is None else v for v in p.values()]
            result_to_write += [curr_params_values + [agg_res['groups_score'], agg_res['special_score']]]
        write_results(out_dir + '/' + classifier, result_to_write)


def run_exp(data_path, classifier_name, parameters, weighted_cols, home_id_indx, away_id_indx, weight=0.5):
    results = {}
    for training_set, testing_set, test_year in utils.get_data_set(data_path, weight, weighted_cols):
        results[test_year] = {}
        clf = get_classifier(classifier_name, parameters)
        x = [val[:-1] for val in training_set]
        y = [int(val[-1]) for val in training_set]
        clf = clf.fit(x, numpy.array(y))
        X_test = [val[:-1] for val in testing_set]
        P = clf.predict(X_test)
        curr_table = {x[0]: 0 for x in X_test}
        for i in range(len(X_test)):
            if int(P[i]) == 1:
                curr_table[X_test[i][home_id_indx]] += 3
            if int(P[i]) == -1:
                curr_table[X_test[i][away_id_indx]] += 3
            if int(P[i]) == 0:
                curr_table[X_test[i][home_id_indx]] += 1
                curr_table[X_test[i][away_id_indx]] += 1

        sorted_table = sorted(curr_table.items(), key=operator.itemgetter(1), reverse=True)
        sorted_table = [[sorted_table.index(i)+1, i[0], i[1]] for i in sorted_table]
        tested_real_table = utils.skip_first_line(utils.get_laliga_table(test_year))
        results[test_year]['groups_score'] = get_groups_evaluate(sorted_table, tested_real_table)
        results[test_year]['special_score'] = get_special_evaluate_results(sorted_table, tested_real_table)
    return results

