from src.common.constants import TABLE_COLS, ORDER, TEAM_ID, POINTS, NUM_OF_TEAMS, LALIGA_TABLE_GROUPS
from src.common import utils

'''
In this part we calculate the score of the expected table comparing with the actual table.
'''
def tables_compare(expected, actual, resolution=1, grouping_weight='uniform'):
    if (NUM_OF_TEAMS % resolution) != 0:
        resolution = 1
    if grouping_weight == 'uniform' or len(grouping_weight) != (NUM_OF_TEAMS / resolution):
        grouping_weight = int((NUM_OF_TEAMS / resolution)) * [1]

    team_id_index = TABLE_COLS.index(TEAM_ID)
    expected = [x[team_id_index] for x in expected]
    actual = [x[team_id_index] for x in actual]
    counter = 0.0
    all_counter = 0.0
    grouped_expected = [p for p in utils.chunks(expected, resolution)]
    grouped_actual= [p for p in utils.chunks(actual, resolution)]
    tables = zip(grouped_expected, grouped_actual, grouping_weight)

    for expected_group, actual_group, w in tables:
        for team_id in expected_group:
            all_counter += w
            if team_id in actual_group:
                counter += w
    return 100 * counter / all_counter


def get_special_groups(table):
    grouped_table = []
    for key, val in LALIGA_TABLE_GROUPS.items():
        curr_group = []
        for p in val:
            curr_group.append(table[p-1])
        grouped_table.append(curr_group)
    return grouped_table


def special_tables_compare(expected, actual, grouping_weight='uniform'):
    if grouping_weight == 'uniform':
        grouping_weight = len(LALIGA_TABLE_GROUPS) * [1]
    team_id_index = TABLE_COLS.index(TEAM_ID)
    expected = [x[team_id_index] for x in expected]
    actual = [x[team_id_index] for x in actual]
    grouped_expected = get_special_groups(expected)
    grouped_actual = get_special_groups(actual)

    counter = 0.0
    all_counter = 0.0
    tables = zip(grouped_expected, grouped_actual, grouping_weight)

    for expected_group, actual_group, w in tables:
        #print(expected_group, actual_group, w)
        for team_id in expected_group:
            all_counter += w
            if team_id in actual_group:
                counter += w
    return 100 * counter / all_counter
