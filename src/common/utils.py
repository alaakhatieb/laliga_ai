'''
utils: DOCS
'''

import csv
from src.common.constants import YEARS, YEARS_PART
from experiments import experiments_config


def read_csv_file(path):
    '''
    READS CSV FILE {PATH}
    '''
    with open(path, 'r') as f:
        data = [row for row in csv.reader(f.read().splitlines())]
    return data


def read_teams_map_file(path):
    with open(path, 'r', encoding='latin-1', errors='ignore') as f:
        data = [row for row in csv.reader(f.read().splitlines())]
    return data


def skip_first_line(data):
    return data[1:]


def get_weighted_data(data_set1, data_set2, w1, w2, indexes):

    if len(data_set1) != len(data_set2):
        return []
    total_data_set = []
    for line1, line2 in zip(data_set1, data_set2):
        total_line = [None] * len(line1)
        for col in range(len(line1)):
            if col in indexes:
                total_line[col] = w1*float(line1[col]) + w2*float(line2[col])
            else:
                total_line[col] = line1[col] # or line2

        total_data_set.append(total_line)
    return total_data_set


def get_data_set(data_path, w, weighted_cols_num):

    for test_year in YEARS:

        training_set = []
        testing_set = []
        for year in YEARS:
            data = {}
            for i in YEARS_PART:
                data[i] = skip_first_line(read_csv_file(data_path + year + '_' + i + '.csv'))
            data = get_weighted_data(data['1'], data['2'], w, 1-w, weighted_cols_num)
            if year == test_year:
                testing_set += data
            else:
                training_set += data
        yield training_set, testing_set, test_year


def get_train2019_dataset():
    #according to experiments: the best dataset is #1:
    data_path = experiments_config.DATA_PATH1
    weighted_cols_num = experiments_config.WEIGHTED1_COLS
    w = 0.5
    dataset = []
    for year in YEARS:
        data = {}
        for i in YEARS_PART:
            data[i] = skip_first_line(read_csv_file(data_path + year + '_' + i + '.csv'))
        data = get_weighted_data(data['1'], data['2'], w, 1 - w, weighted_cols_num)
        dataset += data
    return dataset


def get_2019_dataset_ids_index():
    '''
    returns the index of homeId and awayId cols
    '''

    return experiments_config.DATA1_FIELDS.index("homeId"),\
           experiments_config.DATA1_FIELDS.index("awayId")



def get_team_name_by_id(id):
    teams_map = skip_first_line(read_teams_map_file('../data/teams_maps.csv'))

    for row in teams_map:
        if int(row[0]) == int(id):
            return row[1]


def get_indexes(row, cols):
    return {col : row.index(col) for col in cols}


def filter(rows, col_title, value):
    index = get_indexes(rows[0], [col_title])[col_title]
    return [row for row in rows if(row[index] == value)]


def get_laliga_table(year):
    '''
    :param year: string (09,10,...,18)
    :return: laliga table for this year
    '''

    return read_csv_file('../laliga_tables/order' + year + '.csv')


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def get_2019_dataset():
    return skip_first_line(read_csv_file('../datasets/2019dataset/2019dataset.csv'))
