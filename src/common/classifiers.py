from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from src.common.constants import RANDOM_FOREST, DT, KNN
from sklearn import tree


def get_classifier(classifier_name, parameters):
    if classifier_name == RANDOM_FOREST:
        return RandomForestClassifier(n_estimators=parameters['n_estimators'], criterion=parameters['criterion'],
                                      max_depth=parameters['max_depth'], min_samples_leaf=parameters['min_samples_leaf'],
                                      bootstrap=parameters['bootstrap'], max_features=parameters['max_features'])
    if classifier_name == KNN:
        return KNeighborsClassifier(n_neighbors=parameters['n_neighbors'], weights=parameters['weights'])
    if classifier_name == DT:
        return tree.DecisionTreeClassifier(criterion=parameters['criterion'], splitter=parameters['splitter'], max_depth=parameters['max_depth'],
                                           min_samples_leaf=parameters['min_samples_leaf'])

'''
According to the results of the experiment we build this classifier
'''

def get_2019_classifier():
    return RandomForestClassifier(n_estimators=100, criterion='entropy',
                                  max_depth=100, min_samples_leaf=1,
                                  bootstrap=True)
