'''
Constants
'''

YEARS = ['09', '10', '11', '12', '13', '14', '15', '16', '17', '18'] #07', '08',
YEARS_PART = ['1', '2']
NUM_OF_TEAMS = 20
POST_DEF_LIST = ['RWB', 'LWB', 'LB', 'RB', 'CB']
POS_ATT_LIST = ['LW', 'RW', 'CF', 'ST']
POS_MID_LIST = ['CDM', 'CAM', 'CMD', 'CM', 'RM', 'LM']
POS_GK_LIST = ['GK']
POS_LIST = POST_DEF_LIST + POS_ATT_LIST + POS_GK_LIST
PLAYERS_DIR = 'players_stats'
TEAMS_DIR = 'teams'
RESULTS_DIR = 'results'
PLAYERS_STATS_COLS = ['ID', 'TeamID', 'Pos', 'OVA', 'POT','Attacking', 'Crossing', 'Finishing', 'Short Passing',
                      'Skill', 'Long Passing', 'Ball Control', 'Movement', 'Sprint Speed', 'Reactions', 'Balance',
                      'Power', 'Jumping', 'Long Shots', 'Defending', 'Standing Tackle','Sliding Tackle', 'Goalkeeping',
                      'PAC', 'SHO', 'PAS', 'DRI', 'DEF', 'PHY']
TEAM_COLS = ['ID', 'OVA', 'ATT', 'MID', 'DEF', 'Transfer Budget']

RESULTS_COLS = ['Date', 'HomeTeam', 'AwayTeam', 'FTHG', 'FTAG', 'HTHG', 'HTAG', 'BbAvH', 'BbAvD', 'BbAvA', 'HomeId',
                'AwayId']
TEAMS_ID_COLS = ['ID', 'Name1', 'Name2']
LALIGA_HISTORY_COLS = ['Pos', 'Team', 'ID', 'Seasons', 'Pts', 'GamesPlayed', 'Won', 'Drawn', 'Lost', 'GoalsFor',
                       'GoalsAgainst', '1st', '2nd', '3rd', '4th', '5th', '6th', 'best']
SEASON_COLS = ['Table', 'Year', 'Matches', 'Teams']
RANDOM_FOREST = 'RANDOM_FOREST'
KNN = 'KNN'
DT = 'DT'
DATA_PATH = 'DATA_PATH'
WEIGHTED_COLS = 'WEIGHTED_COLS'
CLASSIFIERS_PARAMS = 'CLASSIFIERS_PARAMS'
DATA_FIELDS = 'DATA_FIELDS'
DATA_PART_WEIGHT = 'DATA_PART_WEIGHT'
ORDER = 'order'
TEAM_ID = 'teamId'
POINTS = 'points'
TABLE_COLS = [ORDER, TEAM_ID, POINTS]
CHAMPION = 'CHAMPION'
UEFA_QUALIFIED = 'UEFA_QUALIFIED'
EUROPA_LEAGUE_QUALIFIED = 'EUROPA_LEAGUE_QUALIFIED'
RELEGATED_TEAMS = 'RELEGATED_TEAMS'
MID_TABLE_TEAMS = 'MID_TABLE_TEAMS'
LALIGA_TABLE_GROUPS = {UEFA_QUALIFIED: [a for a in range(1, 5)],  EUROPA_LEAGUE_QUALIFIED:
    [a for a in range(5, 8)],  MID_TABLE_TEAMS: [a for a in range(8, 18)], RELEGATED_TEAMS: [a for a in range(18, 21)]}
OUT_DIR = 'results'
