from src.common import utils
from src.common.classifiers import get_2019_classifier
from experiments.experiments_config import *
import itertools
from src.result_evaluation import tables_compare, special_tables_compare
import numpy
import os
import operator
import csv

'''
Running our classifier in 2019 season data.
'''

def write_results(out_dir, laliga_table):

    out_file = out_dir + '/' + 'laliga2019_table.csv'
    rank = 1
    with open(out_file, "w") as output:
        writer = csv.writer(output, lineterminator='\n')
        for team_id, points in laliga_table:
            row_to_write = [rank, utils.get_team_name_by_id(team_id), points]
            rank += 1
            writer.writerow(row_to_write)


def run_2019():

    out_dir = '../laliga2019'
    training_dataset = utils.get_train2019_dataset()
    home_id_indx, away_id_indx = utils.get_2019_dataset_ids_index()
    x = [val[:-1] for val in training_dataset]
    y = [int(val[-1]) for val in training_dataset]
    classifier_2019 = get_2019_classifier()
    dataset2019 = utils.get_2019_dataset()
    classifier_2019 = classifier_2019.fit(x, numpy.array(y))
    P = classifier_2019.predict(dataset2019)
    laliga2019_table = {x[0]: 0 for x in dataset2019}
    for i in range(len(dataset2019)):
        if int(P[i]) == 1:
            laliga2019_table[dataset2019[i][home_id_indx]] += 3
        if int(P[i]) == -1:
            laliga2019_table[dataset2019[i][away_id_indx]] += 3
        if int(P[i]) == 0:
            laliga2019_table[dataset2019[i][home_id_indx]] += 1
            laliga2019_table[dataset2019[i][away_id_indx]] += 1

    if not os.path.exists(out_dir):
        os.mkdir(out_dir)

    laliga2019_table = sorted(laliga2019_table.items(), key=operator.itemgetter(1), reverse=True)
    write_results(out_dir, laliga2019_table)

run_2019()